﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {

	public GameObject[] Texts;
	public float Seconds;
	public float StartDelay;
	public float EndDelay;
	
	// privates
	private float timeToChange;
	private int currentText;
	private int state;


	public void Start () {
		for(int i = 0; i < Texts.Length; i++){
			Texts[i].SetActive(false);
		}
		timeToChange = StartDelay;
		currentText = 0;
		state = 0;
		PlayerPrefs.SetInt("siguientePuerta", 99);
	}
	
	void Update ()
	{
		if(state == 0){
			timeToChange -= Time.deltaTime;
			if (timeToChange <= 0) {
				timeToChange = Seconds;
				state++;
				Texts[currentText].SetActive (true);
				currentText++;
			}
		}else if(state == 1){
			timeToChange -= Time.deltaTime;
			if (timeToChange <= 0) {
				if(currentText >= Texts.Length){
					state++;
					timeToChange = EndDelay;
				}else{
					Texts[currentText].SetActive (true);
					currentText++;
					timeToChange = Seconds;
				}
			}
		}else if(state == 2){
			timeToChange -= Time.deltaTime;
			if (timeToChange <= 0) {
				state++;
				Application.LoadLevel(Application.loadedLevel+1);
			}
		}
	}
}
