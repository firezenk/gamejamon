﻿using UnityEngine;
using System.Collections;

public class MusicRecovery : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.audio.time = Globals.getMusicIndex();
		this.audio.Play();
	}

}
