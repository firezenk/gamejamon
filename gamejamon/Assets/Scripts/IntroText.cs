﻿using UnityEngine;
using System.Collections;

public class IntroText : MonoBehaviour {

	public string TextId;

	// Use this for initialization
	public void Start () {
		this.GetComponent<TextMesh> ().text = TextStrings.GetString (TextId);
	}
	
}
