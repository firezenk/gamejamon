﻿#pragma strict

var speed:int;
var distanceMax:float;
var danio:float;
var player:GameObject;
var playerScript:PlayerController;

function Start () {
	player=GameObject.FindWithTag("Player");
	var playerScript=player.GetComponent("PlayerController");
}

function Update () {
	var direccion:Vector3=new Vector3(player.transform.position.x-this.transform.position.x,
										0,
										player.transform.position.z-this.transform.position.z);
	if(direccion.magnitude<4){
		playerScript.estress-=danio;
	}
	if(direccion.magnitude>3||!playerScript.luzOn){
		direccion=direccion.normalized;
		direccion=new Vector3(direccion.x * speed * Time.deltaTime,0, direccion.z * speed * Time.deltaTime);
		this.transform.Translate(direccion);
	}
	
	if (direccion.magnitude<3) {
		direccion.x = - direccion.x;
		direccion.z = - direccion.z;
		this.transform.Translate(direccion);
	}
	
}