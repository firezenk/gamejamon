﻿#pragma strict

private var player:PlayerController;
private var estress:float = 100.0f;
private var audiosource:AudioSource;

public var audio1:AudioClip;
public var audio2:AudioClip;
public var audio3:AudioClip;

function Start () {
	GetComponent("PlayerController");
	var camara:GameObject = GameObject.FindGameObjectWithTag("MainCamera");
	audiosource = camara.GetComponent("AudioSource");
	
	audiosource.PlayOneShot(audio1);
}

function FixedUpdate () {
	estress = player.estress;
	if (estress > 0.129) {
		transform.localScale.x = ((estress*0.12)/0.1)/1000;
	} else {
		Application.LoadLevel("Recibidor");
	}
	
	var es:int = estress;
	
	if (es == 100 && !audio.isPlaying ) {
		audiosource.Stop();
		audiosource.loop = true;
		audiosource.PlayOneShot(audio1);
	} else if (es == 60) {
		audiosource.Stop();
		audiosource.clip = audio2;
		audiosource.PlayDelayed(0.1);
	} else if (es == 30) {
		audiosource.Stop();
		audiosource.clip = audio3;
		audiosource.PlayDelayed(0.1);
	}
	
	if (!audio.isPlaying) {
		audiosource.Play();
	}
}