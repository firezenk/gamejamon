﻿#pragma strict

public var speed:float;
public static var isWalking:boolean = false;
public var tiempoLuz=300;

public var luzOn:boolean; 
private var spriteAbajo:GameObject;
private var spriteArriba:GameObject;
private var spriteDerecha:GameObject;
private var spriteIzquierda:GameObject;

var inventario:GameObject[];
var nextDoor:GameObject;
var hasNextDoor:boolean;

private var inventarioIU:GameObject;
var player:GameObject;

public static var estress:float = 100;

var ESTRESS_LOSS:int;//Estress perdido por segundos
	
public var velaInstance:GameObject;
public var llaveInstance:GameObject;
public var cigarroInstance:GameObject;

function Start ()
{
	//Reinicio variable PlayerPrefs.SetInt("siguientePuerta",-1);
	player = GameObject.FindWithTag("Player");
	
	var almacenamiento:GameObject = GameObject.FindWithTag("Almacenamiento");
	var almacenamientoScript:Almacenamiento= almacenamiento.GetComponent("Almacenamiento");
	
	if(Almacenamiento.instance.saved){
		estress = Almacenamiento.instance.estress;
		tiempoLuz = Almacenamiento.instance.tiempoLuz;
		if(tiempoLuz>0){
			luzOn=true;
		}else{
			luzOn=false;
		}
		
		var almacen:String = Almacenamiento.instance.almacen;
		
		var auxAlmacen:String[]=almacen.Split(":"[0]);
		var i:int=0;
		for(var posi=0;posi<4;posi++){
			var auxs:String = auxAlmacen[posi];
			if(auxs=="null"){
				inventario[posi]=null;
			}else{
				var paramsString:String[]=auxs.Split(","[0]);
				if(paramsString[0]=="Vela"){
					var velaAux:GameObject=Instantiate(velaInstance,Vector3(0,0,0),Quaternion.identity);
					var velaScript:Vela= velaAux.GetComponent("Vela");
					velaScript.duracion=parseFloat(paramsString[1]);
					inventario[i]=velaAux;
				}else if(paramsString[0]=="Llave"){
					var llaveAux:GameObject=Instantiate(llaveInstance,Vector3(0,0,0),Quaternion.identity);
					var llaveScript:Llave= llaveAux.GetComponent("Llave");
					llaveScript.codigo=parseFloat(paramsString[1]);
					inventario[i]=llaveAux;
				}else if(paramsString[0]=="Cigarro"){
					var cigarroAux:GameObject=Instantiate(cigarroInstance,Vector3(0,0,0),Quaternion.identity);
					var cigarroScript:Cigarro= cigarroAux.GetComponent("Cigarro");
					cigarroScript.estressRecuperado=parseFloat(paramsString[1]);
					inventario[i]=cigarroAux;
				}
			}
			i++;
		}
		Almacenamiento.instance.saved=false;
	}
	
	//for(var indexAlmacen=0;indexAlmacen<4;indexAlmacen++){
	//	var go:GameObject=Instantiate(almacenamientoScript.almacen[indexAlmacen])
	//	inventario[indexAlmacen]=go;
	//}
	
	inventarioIU = GameObject.FindWithTag("Inventario");
	
	for(var invObj:GameObject in inventario){
		if(invObj!=null){
			inventarioIU.SendMessage("addElement",invObj);
		}
	}
	
	var externalId:int=PlayerPrefs.GetInt("siguientePuerta");
	
	if(externalId!=-100){
		var listaPuertas:GameObject[]=GameObject.FindGameObjectsWithTag("Puerta");
		//var b:boolean=false;
		for( var p:GameObject in listaPuertas){
			var pu:Puerta = p.GetComponent("Puerta");
			if(pu.codigo == externalId){
				var doorPosition:Vector3 = p.transform.position;
				if(pu.direccion == 1){
					doorPosition.z += 1;
				}else if(pu.direccion == 2){
					doorPosition.x += 1;
				}else if(pu.direccion == 3){
					doorPosition.z -= 1;
				}else{
					doorPosition.x -= 1;
				}
				player.transform.position.x = doorPosition.x;
				player.transform.position.z = doorPosition.z;
			}
		}
		//doorPosition.x += 1;
		//player.transform.position =doorPosition;
		PlayerPrefs.SetInt("siguientePuerta",-1);
		
	}
	

	spriteAbajo = player.Find("SpriteAbajo");
	spriteArriba = player.Find("SpriteArriba");
	spriteDerecha = player.Find("SpriteDerecha");
	spriteIzquierda = player.Find("SpriteIzquierda");
	this.luzOn=false;
	this.hasNextDoor=false;

	GameObject.FindWithTag("Almacenamiento").SendMessage("existo");
}

function Update ()
{
	//luz=this.gameObject.Find("Light");
	var luz:GameObject=GameObject.FindWithTag("PlayerIlumination");
	if(tiempoLuz>0){
		if(!luzOn){
			luzOn=true;	
			luz.light.intensity = 1;
			luz.light.enabled = true;
		}
		tiempoLuz=tiempoLuz-Time.deltaTime;
		if(tiempoLuz<=0){
			tiempoLuz=0;
			luzOn=false;
			luz.light.enabled = false;
			//luz.light=null;
		}			
	}else{
		luz.light.enabled = false;
		estress=estress-Time.deltaTime*2;
	}
	
	if(tiempoLuz<100 && tiempoLuz>=66){
		luz.light.intensity=0.5f;
	}else if(tiempoLuz<66 && tiempoLuz>=33){
		luz.light.intensity=0.7f;
	}else if(tiempoLuz<33 && tiempoLuz>=10){
		luz.light.intensity=0.5f;
	}else if(tiempoLuz<10){
		luz.light.intensity=0.2f;
	}
	
	if(estress<0){
		this.renderer.enabled=false;
		this.collider.isTrigger=false;
	}
}

function FixedUpdate ()
{
	var animAbajo:Animator = spriteAbajo.GetComponent("Animator");
	var animArriba:Animator = spriteArriba.GetComponent("Animator");
	var animDerecha:Animator = spriteDerecha.GetComponent("Animator");
	var animIzquierda:Animator = spriteIzquierda.GetComponent("Animator");

	var moveHorizontal = Input.GetAxis("Horizontal");
	var moveVertical = Input.GetAxis("Vertical");

	if (moveVertical > 0) {
		animArriba.speed = 1;
		spriteAbajo.renderer.enabled=false;
		spriteArriba.renderer.enabled=true;
		spriteIzquierda.renderer.enabled=false;
		spriteDerecha.renderer.enabled=false;
	} else if (moveVertical < 0) {
		animAbajo.speed = 1;
		spriteAbajo.renderer.enabled=true;
		spriteArriba.renderer.enabled=false;
		spriteIzquierda.renderer.enabled=false;
		spriteDerecha.renderer.enabled=false;
	} else {
		animAbajo.speed = 0;
		animArriba.speed = 0;
	}
	
	if (moveHorizontal > 0) {
		animDerecha.speed = 1;
		spriteAbajo.renderer.enabled=false;
		spriteArriba.renderer.enabled=false;
		spriteIzquierda.renderer.enabled=false;
		spriteDerecha.renderer.enabled=true;
	} else if (moveHorizontal < 0) {
		animIzquierda.speed = 1;
		spriteAbajo.renderer.enabled=false;
		spriteArriba.renderer.enabled=false;
		spriteIzquierda.renderer.enabled=true;
		spriteDerecha.renderer.enabled=false;
	} else {
		animDerecha.speed = 0;
		animIzquierda.speed = 0;
	}
	
	if (moveHorizontal == 0 && moveVertical == 0) {
		isWalking = false;
	} else {
		isWalking = true;
	}
	
	var movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
	transform.Translate(movement * speed * Time.deltaTime);
		
	transform.rotation = Quaternion.Euler(90, 0, 0);
}

function addObjects(listaObjetos:GameObject[]){
	for(var i:GameObject in listaObjetos ){
		var colocado=false;
		for(var index=0;index<inventario.length&&!colocado;index++){
			if(inventario[index]==null){
				inventario[index]=i;
				inventarioIU.SendMessage("addElement",i);
				colocado=true;
			}
		}
	}
}

function setNextDoor(other:GameObject ){
	this.hasNextDoor=true;
	this.nextDoor=other;
}

function clearNextDoor(other:GameObject ){
	this.hasNextDoor=false;
	this.nextDoor=other;
}

function testKeyDoor(code: Array){
	if(this.hasNextDoor){
		this.nextDoor.SendMessage("tryOpenDoor", code);
	}
}

function recuperateEstress(code: Array){
	var e:float=code[0];
	estress+=e;
	if(estress> 100) {
		estress = 100;
	}
}

function addTimeLight(time :float){
	tiempoLuz+=time;
}

function nextLevel(arr:Array){
	//arr.push(mapName);arr.push(nextMapName);
	var nombreMapa:String = arr[0];
	var idPuerta:int = arr[1];
	PlayerPrefs.SetInt("siguientePuerta",idPuerta);
	var index: int=0;
	var exist=false;
	
	//var almacenamiento:GameObject = GameObject.FindWithTag("Almacenamiento");
	//var almacenamientoScript:Almacenamiento= almacenamiento.GetComponent("Almacenamiento");
	
	var data:String="";
//	data+=tiempoLuz+":";
//	data+=estress+":";
	
	
	//Almacenamiento.instance.tiempoLuz = tiempoLuz;
	
	
	Almacenamiento.instance.tiempoLuz=tiempoLuz;
	Almacenamiento.instance.estress=estress;
	Almacenamiento.instance.almacen="";

	
	
	//for(var indexAlmacen=0;indexAlmacen<4;indexAlmacen++){
	//	if(inventario[indexAlmacen]!=null){
	//		almacenamientoScript.almacen[indexAlmacen]=Instantiate(inventario[indexAlmacen]);
	//	}else{
	//		almacenamientoScript.almacen[indexAlmacen]=null;
	//	}
	//}
	
	
	//for(var i=0;i<4;i++){
	//	PlayerPrefs.SetString("slot"+index+"Tipo","");
	//	PlayerPrefs.SetString("slot"+index+"Datos","");
	//}
	
	for(var asd=0;asd<4;asd++){
		var o:GameObject =inventario[index];
		var data1:String="";
		if(o!=null){			
			if(o.GetComponent('Vela')!=null){				
	//			PlayerPrefs.SetString("slot"+index+"Tipo",'Vela');
				var vela:Vela =o.GetComponent('Vela');
				data1+="Vela,";
				data1+=vela.duracion+":";
	//			var datos:String=vela.duracion+","+vela.activada;
	//			PlayerPrefs.SetString("slot"+index+"Datos",datos);
	//			Debug.Log("slot"+index+"Datos "+datos);
			}else if(o.GetComponent('Llave')!=null){
				var llave:Llave =o.GetComponent('Llave');
				data1+="Llave,";
				data1+=llave.codigo+":";
	//			var datos1:String=llave.codigo+"";
	//			PlayerPrefs.SetString("slot"+index+"Datos",datos1);
	//			Debug.Log("slot"+index+"Datos "+datos1);
			}else if(o.GetComponent('Cigarro')!=null){
				var cigarro:Cigarro =o.GetComponent('Cigarro');
				data1+="Cigarro,";
				data1+=cigarro.estressRecuperado+":";
			//	var datos:String=cigarro.duracion+","+cigarro.activada;
			//	PlayerPrefs.SetString("slot"+index+"Datos",datos);
			//	PlayerPrefs.SetString("slot"+index+"Tipo",'Cigarro');	
			//	Debug.Log('Cigarro index: '+index);
			}			
		}else{
			data1+="null:";
		}
		data=String.Concat(data,data1);
		index++;
	}
	Debug.Log(data);
	data.Substring(0,data.Length-1);
	 Almacenamiento.instance.almacen = data;
	 Almacenamiento.instance.saved = true;
	 PlayerPrefs.Save();
	//PlayerPrefs.SaveObject("inventario",inventario);
	
	var currentMusicIndex : float = GameObject.FindWithTag("MainCamera").audio.time;
	Globals.setMusicIndex(currentMusicIndex);
	
	Application.LoadLevel(nombreMapa);
}







