using System.Collections;
using System.Collections.Generic;
using System.Text;

public static class TextStrings
{
	/* Textos del juego, en cada idioma soportado */
	private static string[][] data = new string[][]{
	//Inglés
		new string[]{
			"Smoke" ,"Bad moment to be without tobacco",
			"Pay" ,"They don't pay me enough for this",
			"Grass" ,"What am I steping on?",
			"Pain" ,"Ouch! it's hurts",
			"God", "Jesus Christ!",
			"Demons", "What the fuck is this?",
			"Key", "It seems like keys",
			"Candle", "I'm going turn on the candle",
			"Here", "What am I doing here?",
			"Dark", "It's dark",
			"Hello", "Hello",
			"GameSubject", "What I do now?",
			"Yes", "Yes",
			"No", "No",
			"Intro1", "After 10 year working for police, that\nincidence change everything. Now I'm in this\nsituation, few money and so much work.\nBut here I am, I can't change anything.\nMy curren't job is find Ann,\nher parents hired me.",
			"Intro2", "Fortunately, I think I've found her in\nthis village. That black amber hair and that\nflowered dress, which she wears when she\nwas seen last time.",
			"Intro3", "Ann, are you there?... where are you going?...",
			"DoorLocked", "This door is locked. Maybe I need a key",
			"BehindBoxes", "I think I've saw something behind that boxes",
			"LightOff", "My candle is almost consumed"
		},
	//Castellano
		new string[]{
			"Smoke" ,"Mal momento para quedarme sin tabaco",
			"Pay" ,"No me pagan lo suficiente para esto",
			"Grass" ,"¿Que estoy pisando?",
			"Pain" ,"¡Ouch! Eso duele",
			"God", "Dios mio",
			"Demons","¿Que demonios es eso?",
			"Key", "Parecen unas llaves",
			"Candle", "Voy a encender ese candelabro",
			"Here", "¿Qué hago aquí?",
			"Dark", "Está oscuro",
			"Hello", "Hola",
			"GameSubject", "¿Qué hago ahora?",
			"Yes", "Si",
			"No", "No",
			"Intro1", "Tras 10 años como policía, aquel incidente\nlo cambió todo, y me trajo a esta situación;\nun detective que cobra poco y trabaja el triple.\nPero aquí estoy, ya no puedo cambiar nada.\nMi actual trabajo es buscar a Ann.\nSus padres me contrataron para encontrarla.",
			"Intro2", "Aquí estoy, en un pueblo del que\nni siquiera recuerdo el nombre.\nTras un golpe de suerte,\ncreo que he encontrado a Ann.\nUn pelo azabache y aquel vestido de flores\ncon el que se la vio la última vez.",
			"Intro3", "Ann, ¿estás ahí?... ¿eres tu?..."
		}
	/*
		//Para añadir un nuevo idioma
		, new string[]{
			"Identificador1", "Texto1 en el idioma que sea",
			"Identificador2", "Texto2 en el idioma que sea",
			//etc...
			"UltimoIdentificador", "UltimoTexto en el idioma que sea"
		}
		*/
	};
	private static bool init = false;
	private static List<Dictionary<string, string>> dic = new List<Dictionary<string, string>> ();

	public static string GetString (string key)
	{
		if (!init) {
			init = true;
			initData ();
		}

		int index = Globals.GetLanguageIndex ();
		string result;
		try {
			result = dic [index] [key];
		} catch (System.Exception) {
			result = "_" + key + "_";
		}
		return result;
	}

	private static void initData ()
	{
		//Encoding iso = Encoding.GetEncoding("ISO-8859-1");
		//Encoding utf8 = Encoding.UTF8;
		for (int i = 0; i < data.Length; i++) {
			dic.Add (new Dictionary<string, string> ());
			for (int j = 0; j < data[i].Length; j += 2) {
				string key = data [i] [j];
				//string text = utf8.GetString(iso.GetBytes(data [i] [j + 1]));
				string text = data [i] [j + 1];
				dic [i].Add (key, text);
			}
		}
	}
}
