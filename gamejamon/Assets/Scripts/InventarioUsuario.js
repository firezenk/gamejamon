﻿#pragma strict

var listado :GameObject[];
var isModified:boolean;

function Start () {
	isModified=true;
}

function Update () {
	if(isModified){
		for(var i=0; i<listado.length;i++){	
			if(listado[i]!=null){
				var offset:float;
				switch(i){
					case 0:
						offset=-1.5f;
					break;
					case 1:
						offset=-0.5f;
					break;
					case 2:
						offset=0.5f;
					break;
					case 3:
						offset=1.5f;
					break;
				}	
				Debug.Log((this.transform.position.x-listado[i].transform.position.x+offset)+","+(this.transform.position.y-listado[i].transform.position.y)+","+(this.transform.position.z-listado[i].transform.position.z));
				
				listado[i].transform.Translate(Vector3(this.transform.position.x-listado[i].transform.position.x+offset,
												this.transform.position.y-listado[i].transform.position.y,this.transform.position.z-listado[i].transform.position.z));	
				listado[i].transform.parent=this.gameObject.transform;
				//listado[i].renderer.enabled=true;
				//listado[i].collider.isTrigger=false;
			}
		}
		isModified=false;
	}
}

function addElement(obj:GameObject){
	var setted=false;
	for(var i=0; i<listado.length && !setted;i++){
		if(listado[i]==null){
			listado[i]=obj;
			isModified=true;
			setted=true;
		}
	}
}