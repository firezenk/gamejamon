﻿#pragma strict

public var walkOne:AudioClip;
public var walkTwo:AudioClip;
public var walkThree:AudioClip;
public var walkFour:AudioClip;

public var trueno:GameObject;

private var random:int;

private var player:PlayerController;

private var time:int = 30;
private var truenoTime = 400;

private var truenoInstance:GameObject;

function Start () {
	player = GetComponent("PlayerController");
}	

function FixedUpdate () {
	
	random = Random.Range(1, 4);
	
	if(player.isWalking && time <= 0) {
		switch (random) {
			case 1:
				audio.PlayOneShot(walkOne);
			break;
			case 2:
				audio.PlayOneShot(walkTwo);
			break;
			case 3:
				audio.PlayOneShot(walkThree);
			break;
			case 4:
				audio.PlayOneShot(walkFour);
			break;
		}
		time = 30;
	} else {
		time--;
	}
	
	if (truenoTime <= 0) {
		truenoInstance = Instantiate(trueno, Vector3(0,0,0), Quaternion.identity);
		truenoTime = Random.Range(400, 800);
		Destroy(truenoInstance, 3);
	} else {
		truenoTime--;
	}

}