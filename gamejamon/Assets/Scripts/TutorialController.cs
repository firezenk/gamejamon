using UnityEngine;
using System.Collections;

public class TutorialController : MonoBehaviour
{		
	public GameObject dialog;
	public string tutorialText;
	public float Seconds;
	public bool destroyAfterShow;
	
	// privates
	private	bool mustDestroy = false;
	private float timeToDestroy;
	
	// Método que muestra un cuadro de diálogo los segundos indicados
	public void ShowDialog (float seconds)
	{
		//Informamos en caso de que se solapen varios mensajes
		if (dialog.activeSelf) {
			throw new System.Exception ("HUD dialog is already active");
		}
		
		//Mostramos dialogo
		dialog.SetActive (true);
		Debug.Log("setActive");
		
		//Contenidos
		dialog.transform.FindChild ("DialogText").GetComponent<TextMesh> ().text = TextStrings.GetString (tutorialText);
		
		//Eliminacion
		if (seconds > 0) {
			HideDialog (seconds);
		}
	}
	
	// Método que cambia el texto a mostrar
	public void ChangeText (string dialogText)
	{
		tutorialText = dialogText;
	}

	public void HideDialog (float time)
	{
		if (!mustDestroy) {
			timeToDestroy = time;
			mustDestroy = true;
		}
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			ShowDialog (Seconds);
		}
	}
	
	void Update ()
	{
		//if (Globals.gameActive) {
			//destruccion del objeto
			if (mustDestroy) {
				timeToDestroy -= Time.deltaTime;
				if (timeToDestroy <= 0) {
					dialog.SetActive (false);
					if(destroyAfterShow){
						Destroy (gameObject);
					}
				}
			}
		//}
	}
}