using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public static class Globals
{
	/* Idiomas */
	public static Language lang = Language.English; // Indica si el juego esta activo

	//Devuelve el indice asociado al idioma (en que está configurado el juego actualmente)
	public static int GetLanguageIndex ()
	{
		return (int)lang;
	}
	
	// Metodo que devuelve la descripcion del tipo enumerado seleccionado
	public static string GetEnumDescription (Enum enumValue)
	{
		FieldInfo fi = enumValue.GetType ().GetField (enumValue.ToString ());
		DescriptionAttribute[] attribute = (DescriptionAttribute[])fi.GetCustomAttributes (typeof(DescriptionAttribute), false);
		return attribute [0].Description;
	}

	/* Musica */
	private static float musicIndex = 0;

	public static float getMusicIndex(){
		return musicIndex;
	}

	public static void setMusicIndex(float index){
		musicIndex = index;
	}

	/* Puertas */
	private static List<int> unlockedDoors = new List<int>();
	
	public static bool isUnlockedDoor(int doorId){
		return unlockedDoors.Contains(doorId);
	}

}

/* Idiomas */
public enum Language
{
	[Description("EN")]
	English = 0,
	
	[Description("ES")]
	Spanish = 1
}
